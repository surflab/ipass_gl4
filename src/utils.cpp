#ifndef UTILS_CPP
#define UTILS_CPP

#include "common.h"
#include "utils.h"
#include "surface.h"

#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <exception>

using namespace std;

vector<glm::ivec4> calConnectivityOfPatches(Vertex* vertices, int num_vertices, GLuint* indices, int num_indices)
{
	float eps = 0.00001;
	int edge_point_idices[4][4] = {	{0,1,2,3},		// top row
									{12,13,14,15},	// bottom row
									{0,4,8,12},		// left column
									{3,7,11,15}		// right column
								};

	// 16 vertex per patch, hard coded
	int num_patches = num_indices / 16;

	// connectivity array
	vector<glm::ivec4> conn(num_patches, glm::ivec4(-1,-1,-1,-1));

	// TODO: should use hash table for big pathes, avoid linear search
	vector<glm::vec4> vertices_table;
	
	// array of <patch_id, edge_id> to record which edge this vertex belong to
	vector<vector<pair<int, int> > > vertices_location; 


	for(int patch_idx = 0; patch_idx < num_patches; patch_idx++)
	{
		for(int edge_idx = 0; edge_idx < 4; edge_idx++)
		{
			for(int vidx = 0; vidx < 4; vidx++)
			{
				glm::vec4 pos = vertices[indices[patch_idx * 16 + edge_point_idices[edge_idx][vidx]]].XYZW;
				// glm::vec4 pos(pos_ptr[0], pos_ptr[1], pos_ptr[2], pos_ptr[3]);

				int found_idx = -1;
				// search pos in vertices table
				for(int i = 0; i < vertices_table.size(); i++)
				{
					if(glm::distance(vertices_table[i], pos) < eps)
					{
						found_idx = i;
						break;
					}
				}

				if(found_idx == -1) // not found
				{
					vertices_table.push_back(pos);
					vertices_location.push_back(vector<pair<int,int> >());
					found_idx = vertices_table.size() - 1;
				}

				vertices_location[found_idx].push_back(make_pair(patch_idx, edge_idx));	

				// cout << "v:" << found_idx << " patch_id:" << patch_idx << " edge_id:" << edge_idx << endl;
			} // for each vidx
		} // for each edge_idx
	} // for each patch_idx

	// connect each pair of patches connect by a point
	for(int vidx = 0; vidx < vertices_table.size(); vidx++)
	{
		// only deal with point within the edge
		if(vertices_location[vidx].size() != 2)
			continue;

		for(int i = 0; i < vertices_location[vidx].size(); i++)
		{
			pair<int,int> location = vertices_location[vidx][i];
			for(int j = i+1;  j < vertices_location[vidx].size(); j++)
			{
				pair<int,int> next_location = vertices_location[vidx][j];

				// prevent self connection caused by corner vertex
				if(location.first == next_location.first)
					continue;
				
				if(conn[location.first][location.second] == -1)
				{
					conn[location.first][location.second] = next_location.first;
				}
				else
				{
					assert(conn[location.first][location.second] == next_location.first);
				}


				if(conn[next_location.first][next_location.second] == -1)
				{
					conn[next_location.first][next_location.second] = location.first;
				}
				else
				{
					assert(conn[next_location.first][next_location.second] == location.first);
				}
			}
			
		}
	}

	return conn;
}


void panCamera(Camera* camera, glm::vec2 offset)
{
	glm::vec3 vFront = glm::normalize(camera->center - camera->pos);
	glm::vec3 vUp = glm::normalize(camera->up);
	glm::vec3 vRight = glm::cross(vFront,vUp);
	vUp = glm::cross(vRight,vFront);

	glm::vec3 offset3 = (offset.x * vRight + offset.y * vUp) * tan(camera->fovy) * glm::length(camera->center - camera->pos);

	camera->center = camera->center + offset3;
	camera->pos = camera->pos + offset3;
}

void rotateCamera(Camera* camera, glm::vec2 angle){
	glm::vec3 vFront = glm::normalize(camera->center - camera->pos);
	glm::vec3 vUp = glm::normalize(camera->up);
	glm::vec3 vRight = glm::cross(vFront,vUp);
	vUp = glm::cross(vRight,vFront);

	glm::mat4 tMatrix = glm::translate(camera->center);
	//glm::mat4 tMatrix(1.0);
	tMatrix = glm::rotate(tMatrix,-angle.y,vRight);
	tMatrix = glm::rotate(tMatrix,angle.x, vUp);
	tMatrix = glm::translate(tMatrix, -1.0f*camera->center);

	camera->pos = (tMatrix*glm::vec4(camera->pos,1.0)).xyz();

}

void zoomCamera(Camera* camera, float factor){
	glm::vec3 vFront = camera->pos - camera->center;
	vFront = factor * vFront;
	camera->pos = vFront + camera->center;
}

void forwadCamera(Camera* camera, float offset)
{
	glm::vec3 vFront = glm::normalize(camera->center - camera->pos);

	camera->center = camera->center + offset * vFront;
	camera->pos = camera->pos + offset * vFront;
}

#endif
