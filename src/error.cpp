#include "common.h"
#include "error.h"
#include <cstring>

using namespace std;

#ifdef APIENTRY
static void APIENTRY debugOutput
#else
static void debugOutput
#endif
(
	GLenum source,
	GLenum type,
	GLuint id,
	GLenum severity,
	GLsizei length,
	const GLchar* message,
	GLvoid* userParam
)
//typedef void (APIENTRY *GLDEBUGPROCARB)(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, GLvoid* userParam);
{
	//FILE* f;
	//f = fopen("debug_output.txt","a");
	//if(f)
	{
		char debSource[32], debType[32], debSev[32];
		bool Error(false);

		if(source == GL_DEBUG_SOURCE_API_ARB)
			strcpy(debSource, "OpenGL");
		else if(source == GL_DEBUG_SOURCE_WINDOW_SYSTEM_ARB)
			strcpy(debSource, "Windows");
		else if(source == GL_DEBUG_SOURCE_SHADER_COMPILER_ARB)
			strcpy(debSource, "Shader Compiler");
		else if(source == GL_DEBUG_SOURCE_THIRD_PARTY_ARB)
			strcpy(debSource, "Third Party");
		else if(source == GL_DEBUG_SOURCE_APPLICATION_ARB)
			strcpy(debSource, "Application");
		else if(source == GL_DEBUG_SOURCE_OTHER_ARB)
			strcpy(debSource, "Other");
 
		if(type == GL_DEBUG_TYPE_ERROR_ARB)
			strcpy(debType, "error");
		else if(type == GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR_ARB)
			strcpy(debType, "deprecated behavior");
		else if(type == GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR_ARB)
			strcpy(debType, "undefined behavior");
		else if(type == GL_DEBUG_TYPE_PORTABILITY_ARB)
			strcpy(debType, "portability");
		else if(type == GL_DEBUG_TYPE_PERFORMANCE_ARB)
			strcpy(debType, "performance");
		else if(type == GL_DEBUG_TYPE_OTHER_ARB)
			strcpy(debType, "message");
 
		if(severity == GL_DEBUG_SEVERITY_HIGH_ARB)
		{
			strcpy(debSev, "high");
			Error = true;
		}
		else if(severity == GL_DEBUG_SEVERITY_MEDIUM_ARB)
			strcpy(debSev, "medium");
		else if(severity == GL_DEBUG_SEVERITY_LOW_ARB)
			strcpy(debSev, "low");

			fprintf(stderr,"%s: %s(%s) %d: %s\n", debSource, debType, debSev, id, message);
			//assert(!Error);
			//fclose(f);
	}
}

bool initDebugOutput()
{
	if (GLEW_KHR_debug)
	{
		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
		glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DEBUG_SEVERITY_MEDIUM, 0, NULL, GL_TRUE);
		glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DEBUG_SEVERITY_HIGH, 0, NULL, GL_TRUE);
		glDebugMessageCallback(&debugOutput, NULL);

	}

	return checkError("initDebugOutput");
}
