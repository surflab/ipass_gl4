#ifndef GLRESOURCE_H
#define GLRESOURCE_H

#include "common.h"

namespace GLResource{
	enum GLResourceType{
		INT, UINT, FLOAT, FLOAT4, INT4
	};

	inline int getDataSize(GLResourceType type)
	{
		switch(type)
		{
		case INT:
			return sizeof(int);
		case UINT:
			return sizeof(unsigned int);
		case FLOAT:
			return sizeof(float);
		case FLOAT4:
			return sizeof(float) * 4;
		case INT4:
			return sizeof(int) * 4;
		default:
			return 0;
		}
	}
};

class GLResourceBase
{
protected:
	GLuint _handle;
public:
	GLResourceBase() : _handle(0)
	{
	}

	GLResourceBase(GLuint handle) : _handle(handle)
	{
	}

	GLuint getHandle()
	{
		return _handle;
	}
};

class GLTexture: public GLResourceBase
{
protected:
	GLResource::GLResourceType _type;

public: 

	GLTexture(GLuint handle, GLResource::GLResourceType type): GLResourceBase(handle), _type(type)
	{
	}

	GLResource::GLResourceType getType()
	{
		return _type;
	}

	virtual int getTextureDataSize() = 0;
	virtual void dumpTextureValue(void* data,  GLuint dataType) = 0;


};

class GLTexture2D: public GLTexture
{
private:
	int _width, _height;

public:

	GLTexture2D(GLuint handle,  GLResource::GLResourceType type, int width, int height)
		: GLTexture(handle, type), _width(width), _height(height)
	{
	}

	virtual int getTextureDataSize()
	{
		return GLResource::getDataSize(_type) * _width * _height;
	}

	virtual void dumpTextureValue(void* data, GLuint dataType)
	{
		glBindTexture(GL_TEXTURE_2D, _handle);
		switch(_type)
		{
		case GLResource::INT:
			glGetTexImage(GL_TEXTURE_2D,0,GL_RED_INTEGER,dataType,data);
			break;
		case GLResource::UINT:
			glGetTexImage(GL_TEXTURE_2D,0,GL_RED_INTEGER,dataType,data);
			break;
		case GLResource::FLOAT:
			glGetTexImage(GL_TEXTURE_2D,0,GL_RED,dataType,data);
			break;
		case GLResource::FLOAT4:
			glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA,dataType,data);
			break;
		case GLResource::INT4:
			glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA_INTEGER,dataType,data);
			break;
		}
		glBindTexture(GL_TEXTURE_2D, 0);
		
	}
};

class GLTexture1D: public GLTexture
{
private:
	int _width;

public:

	GLTexture1D(GLuint handle,  GLResource::GLResourceType type, int width)
		: GLTexture(handle, type), _width(width)
	{
	}

	virtual int getTextureDataSize()
	{
		return GLResource::getDataSize(_type) * _width;
	}

	virtual void dumpTextureValue(void* data, GLuint dataType)
	{
		glBindTexture(GL_TEXTURE_1D, _handle);
		switch(_type)
		{
		case GLResource::INT:
			glGetTexImage(GL_TEXTURE_1D,0,GL_RED_INTEGER,dataType,data);
			break;
		case GLResource::UINT:
			glGetTexImage(GL_TEXTURE_1D,0,GL_RED_INTEGER,dataType,data);
			break;
		case GLResource::FLOAT:
			glGetTexImage(GL_TEXTURE_1D,0,GL_RED,dataType,data);
			break;
		case GLResource::FLOAT4:
			glGetTexImage(GL_TEXTURE_1D,0,GL_RGBA,dataType,data);
			break;
		case GLResource::INT4:
			glGetTexImage(GL_TEXTURE_1D,0,GL_RGBA_INTEGER,dataType,data);
			break;
		}
		glBindTexture(GL_TEXTURE_1D, 0);
		
	}
};

class GLBuffer : public GLResourceBase
{
protected:
	int _size;
public:
	GLBuffer(GLuint handle, int size): GLResourceBase(handle), _size(size) 
	{
	}

	~GLBuffer()
	{
		if(this->_handle != 0)
			glDeleteBuffers(1, &this->_handle);
	}
	
	void dumpData(void* data)
	{
		void* mappedData = NULL;
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, this->_handle);
		glGetBufferSubData(GL_SHADER_STORAGE_BUFFER, 0, this->_size, data);
	}

	void clearData()
	{
		unsigned int zero = 0;
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, this->getHandle());	
		glClearBufferData(GL_SHADER_STORAGE_BUFFER, GL_R32UI, GL_RED_INTEGER, GL_UNSIGNED_INT,&zero);
	}

	void bind(GLenum target, GLuint index)
	{
		glBindBuffer(target, this->getHandle());	
		glBindBufferBase(target, index, this->getHandle());
	}

	void bindRange(GLenum target, GLuint index, GLintptr offset, GLsizeiptr size)
	{
		glBindBuffer(target, this->getHandle());	
		glBindBufferRange(target, index, this->getHandle(), offset, size);
	}
};

GLTexture* createComputeTexture2DMSAA(int width, int height, GLResource::GLResourceType type);

GLTexture* createComputeTexture2D(int width, int height, GLResource::GLResourceType type);
GLTexture* createComputeTexture1D(int width, GLResource::GLResourceType type);

GLBuffer* createBuffer(int size, GLuint usage);

void CreateVBO(Vertex* Vertices, int num_vertices, GLuint* Indices, int num_indices, GLuint *VaoId, GLuint *BufferId, GLuint *IndexBufferId);
void DestroyVBO(GLuint *BufferId, GLuint *IndexBufferId);

#endif