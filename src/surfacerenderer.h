#ifndef SURFACE_RENDERER_H
#define SURFACE_RENDERER_H

#define ZONE_ID_RENDERGROUP 0
#define ZONE_ID_IPASS 1
#define ZONE_ID_RENDER 2
#define ZONE_ID_SCANLINE 3
#define ZONE_ID_SORT 4
#define ZONE_ID_STRIP 5

#define ZONE_ID_END 6


#include <vector>
#include "surface.h"


struct PatchBuffer {
	float TessLevel[MAX_NUM_PATCH];
};

struct SlefeBox
{
	vec4 upper;
	vec4 lower;
};

class SurfaceRenderer
{
public:
	static GLSLProgram render_pass;		// rendering pass
	static GLSLProgram patch_tess_pass;		// determine patch tessellation level


	// GL constant
	static int max_group_count_x;

	static void init_shaders();

private:

	GLBuffer* patch_buffer;	    // store the per patch tessellation level

public:
	SurfaceRenderer()
	{
		init_gpu_resource();
	}

	~SurfaceRenderer()
	{
	}
	
	void init_gpu_resource()
	{
		// resource for compute shader		
		patch_buffer = createBuffer(sizeof(PatchBuffer), GL_STATIC_DRAW);

	}

	void renderSurface(GLSurface* surface, int CurrentWidth, bool is_first_frame, bool use_compute, 
					   glm::vec3 Ka, glm::vec3 Kd, glm::vec3 Ks,
					   const glm::mat4 &ModelViewMatrix, const glm::mat4 &ProjectionMatrix);
	
private:
	
	void updateIPASSTexture(GLSurface *surface, const glm::mat4 &MVP, float pixel_size);
	void updateIPASSTexture_CS(int num_vertices, GLuint vbuffer_id, GLuint patch_tess_level_texture_id, const glm::mat4 &MVP, float pixel_size);
};

#endif
