# Overview

This is a Opengl 4 implementation of the iPass algorithm[1] for bi-cubic Bezier patches.

> [1] Young In Yeo, Lihan Bin, and Jorg Peters. 2012. Efficient pixel-accurate rendering of curved surfaces. In Proceedings of the ACM SIGGRAPH Symposium on Interactive 3D Graphics and Games (I3D '12).

# Dependency

This program requires several enternal library

* __glm__, for basic linear algebra.
* __freeglut__, for cross platform window nad I/O handling.
* __GLEW__, OpenGL extension.
* __ANtTweakBar__, basic UI.

# Build

## Linux

Use the make file under '/build'. You should make sure the system has all the required library installed.

## Windows

Use the Visual Studio project file are located at `/build/VS2010`. All the .lib and .dll files are included. 

# Usage

key bindings:

* __'c'__, switch between gpu and cpu for tessellation factor computation.
* __'F/f'__, turn on/off wireframe mode.

# LICENSE

Copyright (C) 2014  Ruijin Wu <ruijin@cise.ufl.edu>, University of Florida

The source code is licensed under GPL v3. License is available [here](/LICENSE).
