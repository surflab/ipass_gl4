#include "surfacerenderer.h"
#include "patch.h"

#include <numeric>
#include <fstream>
using namespace std;

GLSLProgram SurfaceRenderer::render_pass;
GLSLProgram SurfaceRenderer::patch_tess_pass;


int SurfaceRenderer::max_group_count_x = 65536;


void SurfaceRenderer::init_shaders()
{
	glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 0, &SurfaceRenderer::max_group_count_x);
	cout << "GL_MAX_COMPUTE_WORK_GROUP_COUNT[0]: " << SurfaceRenderer::max_group_count_x << endl;

	int max_texture_size = 0;
	glGetIntegerv(GL_MAX_TEXTURE_SIZE , &max_texture_size);
	cout << "GL_MAX_TEXTURE_SIZE: " << max_texture_size << endl;

	GLint64  msize;
	glGetInteger64i_v(GL_SHADER_STORAGE_BUFFER_SIZE, 1, &msize);
	glGetInteger64v(GL_MAX_SHADER_STORAGE_BLOCK_SIZE, &msize);
	cout << "GL_SHADER_STORAGE_BUFFER_SIZE: " << msize << endl;


	// load rendering shader
	{
		cout << "compiling shader render_pass" << endl;
		ADD_SHADER(render_pass, "../shader/render_pass.vs.glsl", GLSLShader::VERTEX);
		ADD_SHADER(render_pass, "../shader/render_pass.ps.glsl", GLSLShader::FRAGMENT);
		ADD_SHADER(render_pass, "../shader/render_pass.tc.glsl", GLSLShader::TESS_CONTROL);
		ADD_SHADER(render_pass, "../shader/render_pass.te.glsl", GLSLShader::TESS_EVALUATION);
		LINK_PROGRAM(render_pass);
	}

	// load compute shader for tessellation level calculation
	{
		cout << "compiling shader patch_tess_pass" << endl;
		ADD_SHADER(patch_tess_pass, "../shader/patch_tess_pass.cs.glsl", GLSLShader::COMPUTE);
		LINK_PROGRAM(patch_tess_pass);
	}
}



void SurfaceRenderer::renderSurface(GLSurface *surface, int CurrentWidth, bool is_first_frame, bool use_compute, 
					   glm::vec3 Ka, glm::vec3 Kd, glm::vec3 Ks,
					   const glm::mat4 &ModelViewMatrix, const glm::mat4 &ProjectionMatrix)
{
	float pixel_size = 2.0/ CurrentWidth; // pixel size in clipping space

	if(use_compute)
		updateIPASSTexture_CS(surface->num_vertices, surface->vbuffer_id, 0, ProjectionMatrix * ModelViewMatrix, pixel_size);	
	else
		updateIPASSTexture(surface, ProjectionMatrix * ModelViewMatrix, pixel_size);
	
	render_pass.use();
	render_pass.setUniform("ModelViewMatrix", ModelViewMatrix);
	render_pass.setUniform("ProjectionMatrix", ProjectionMatrix);
	render_pass.setUniform("NormalMatrix", glm::inverseTranspose(glm::mat3(ModelViewMatrix)));
	
	render_pass.setUniform("Ka", Ka);
	render_pass.setUniform("Ks", Ks);

	if(surface->selected)
		render_pass.setUniform("Kd", vec3(0,0,1));
	else
		render_pass.setUniform("Kd", Kd);

	glActiveTexture(GL_TEXTURE5);
	glBindTexture(GL_TEXTURE_1D, surface->connectivity_texture->getHandle());

	this->patch_buffer->bind(GL_SHADER_STORAGE_BUFFER, 2);

	glBindVertexArray(surface->vao_id);
	glBindBuffer(GL_ARRAY_BUFFER, surface->vbuffer_id);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, surface->ibuffer_id);

	glPatchParameteri(GL_PATCH_VERTICES, 16);
	glDrawElements(GL_PATCHES, surface->num_vertices, GL_UNSIGNED_INT, 0);

}

void SurfaceRenderer::updateIPASSTexture(GLSurface *surface, const glm::mat4 &MVP, float pixel_size)
{
	vector<float> patch_tess_levels = determine_patch_tess_levels(&surface->patch_vertices[0], surface->num_patches, 3,3, MVP, pixel_size);

	glBindBuffer(GL_SHADER_STORAGE_BUFFER, this->patch_buffer->getHandle());
	glBufferSubData(GL_SHADER_STORAGE_BUFFER, 0, patch_tess_levels.size() * sizeof(float), &patch_tess_levels[0]);
}


void SurfaceRenderer::updateIPASSTexture_CS(int num_vertices, GLuint vbuffer_id, GLuint patch_tess_level_texture_id, const glm::mat4 &MVP, float pixel_size)
{

	this->patch_tess_pass.use();
	
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, vbuffer_id); 
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, vbuffer_id); 

	this->patch_buffer->bind(GL_SHADER_STORAGE_BUFFER, 1);

	this->patch_tess_pass.setUniform("num_vertices", num_vertices);
	this->patch_tess_pass.setUniform("pixel_size", pixel_size);
	this->patch_tess_pass.setUniform("MVP", MVP);

	int groupSize = 32;
	glDispatchCompute(num_vertices/groupSize + 1,1,1);
}




