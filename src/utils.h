#ifndef UTILS_H
#define UTILS_H

#include "common.h"

std::vector<glm::ivec4> calConnectivityOfPatches(Vertex* vertices, int num_vertices, GLuint* indices, int num_indices);


void panCamera(Camera* camera, glm::vec2 offset);
void rotateCamera(Camera* camera, glm::vec2 angle);
void zoomCamera(Camera* camera, float factor);
void forwadCamera(Camera* camera, float offset);

#endif