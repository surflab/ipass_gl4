/*
 *  OpenGL 4 Sample	1
 *	Author: Ruijin Wu <ruijin@cise.ufl.edu>
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <fstream>

#include <AntTweakBar.h>

using namespace std;

#include "setting.h"
#include "common.h"
#include "utils.h"
#include "error.h"

#include "glslprogram.h"
#include "glresource.h"

#include "patch.h"
#include "surface.h"
#include "surfacerenderer.h"



int CurrentWidth = 1000,
	CurrentHeight = 1000,
	WindowHandle = 0;

glm::mat4 ModelMatrix;
glm::mat4 ViewMatrix;
glm::mat4 ProjectionMatrix;

glm::vec3 Ka = glm::vec3(0.2, 0.2, 0.2);
glm::vec3 Kd = glm::vec3(0.9, 0.8, 0.2);
glm::vec3 Ks = glm::vec3(0.5, 0.5, 0.5);

bool use_compute = true;
bool show_ui = true;

GLSurface* test_surface;
SurfaceRenderer* renderer;

Camera camera;


bool debug_mode = false;
bool wireframe_mode = false;

bool is_first_frame = true;


// test data

Vertex test_patch_vertices[] = { 
{ vec4(0.0834088,0,-0.0859713,1), vec4(0,0,0,0), vec4(1,0.5,0,0)},
{ vec4(0.0609865,0.00736679,-0.0829743,1), vec4(0,0,0,0), vec4(0.865588,0.544161,0,0)},
{ vec4(0.0431156,0.0171481,-0.0799152,1), vec4(0,0,0,0), vec4(0.75846,0.602795,0,0)},
{ vec4(0.0290158,0.0290158,-0.0767018,1), vec4(0,0,0,0), vec4(0.673937,0.673937,0,0)},
{ vec4(0.0787311,-0.00736679,-0.0652296,1), vec4(0,0,0,0), vec4(0.971959,0.455839,0,0)},
{ vec4(0.0550038,0,-0.0619834,1), vec4(0,0,0,0), vec4(0.829724,0.5,0,0)},
{ vec4(0.0362691,0.00981342,-0.0581055,1), vec4(0,0,0,0), vec4(0.717418,0.558827,0,0)},
{ vec4(0.0212823,0.0212823,-0.0544352,1), vec4(0,0,0,0), vec4(0.627578,0.627578,0,0)},
{ vec4(0.0735299,-0.0171481,-0.0495009,1), vec4(0,0,0,0), vec4(0.94078,0.397205,0,0)},
{ vec4(0.0477143,-0.00981342,-0.0466603,1), vec4(0,0,0,0), vec4(0.786027,0.441173,0,0)},
{ vec4(0.0277647,0,-0.043559,1), vec4(0,0,0,0), vec4(0.666438,0.5,0,0)},
{ vec4(0.011279,0.011279,-0.0401258,1), vec4(0,0,0,0), vec4(0.567613,0.567613,0,0)},
{ vec4(0.0677865,-0.0290158,-0.0379312,1), vec4(0,0,0,0), vec4(0.906351,0.326063,0,0)},
{ vec4(0.0399525,-0.0212823,-0.035765,1), vec4(0,0,0,0), vec4(0.739498,0.372422,0,0)},
{ vec4(0.0182498,-0.011279,-0.033155,1), vec4(0,0,0,0), vec4(0.6094,0.432387,0,0)},
{ vec4(0,0,-0.0300375,1), vec4(0,0,0,0), vec4(0.5,0.5,0,0)},
{ vec4(0,0.0834088,-0.0859713,1), vec4(0,0,0,0), vec4(0.5,1,0,0)},
{ vec4(-0.00736679,0.0787311,-0.0652296,1), vec4(0,0,0,0), vec4(0.455839,0.971959,0,0)},
{ vec4(-0.0171481,0.0735299,-0.0495009,1), vec4(0,0,0,0), vec4(0.397205,0.94078,0,0)},
{ vec4(-0.0290158,0.0677865,-0.0379312,1), vec4(0,0,0,0), vec4(0.326063,0.906351,0,0)},
{ vec4(0.00736679,0.0609865,-0.0829743,1), vec4(0,0,0,0), vec4(0.544161,0.865588,0,0)},
{ vec4(0,0.0550038,-0.0619834,1), vec4(0,0,0,0), vec4(0.5,0.829724,0,0)},
{ vec4(-0.00981342,0.0477143,-0.0466603,1), vec4(0,0,0,0), vec4(0.441173,0.786027,0,0)},
{ vec4(-0.0212823,0.0399525,-0.035765,1), vec4(0,0,0,0), vec4(0.372422,0.739498,0,0)},
{ vec4(0.0171481,0.0431156,-0.0799152,1), vec4(0,0,0,0), vec4(0.602795,0.75846,0,0)},
{ vec4(0.00981342,0.0362691,-0.0581055,1), vec4(0,0,0,0), vec4(0.558827,0.717418,0,0)},
{ vec4(0,0.0277647,-0.043559,1), vec4(0,0,0,0), vec4(0.5,0.666438,0,0)},
{ vec4(-0.011279,0.0182498,-0.033155,1), vec4(0,0,0,0), vec4(0.432387,0.6094,0,0)},
{ vec4(0.0290158,0.0290158,-0.0767018,1), vec4(0,0,0,0), vec4(0.673937,0.673937,0,0)},
{ vec4(0.0212823,0.0212823,-0.0544352,1), vec4(0,0,0,0), vec4(0.627578,0.627578,0,0)},
{ vec4(0.011279,0.011279,-0.0401258,1), vec4(0,0,0,0), vec4(0.567613,0.567613,0,0)},
{ vec4(0,0,-0.0300375,1), vec4(0,0,0,0), vec4(0.5,0.5,0,0)},
{ vec4(-0.0834088,0.0751651,-0.0108063,1), vec4(0,0,0,0), vec4(0,0.950582,0,0)},
{ vec4(-0.0787311,0.0474252,-0.0104377,1), vec4(0,0,0,0), vec4(0.028041,0.784293,0,0)},
{ vec4(-0.0735299,0.0225143,-0.00983858,1), vec4(0,0,0,0), vec4(0.0592201,0.634963,0,0)},
{ vec4(-0.0677865,-2e-012,-0.00891536,1), vec4(0,0,0,0), vec4(0.0936493,0.5,0,0)},
{ vec4(-0.0609865,0.0725366,-0.0178045,1), vec4(0,0,0,0), vec4(0.134412,0.934826,0,0)},
{ vec4(-0.0550038,0.044813,-0.0171704,1), vec4(0,0,0,0), vec4(0.170276,0.768635,0,0)},
{ vec4(-0.0477143,0.0208421,-0.0160048,1), vec4(0,0,0,0), vec4(0.213973,0.624939,0,0)},
{ vec4(-0.0399525,0,-0.0144827,1), vec4(0,0,0,0), vec4(0.260502,0.5,0,0)},
{ vec4(-0.0431156,0.0700766,-0.0269866,1), vec4(0,0,0,0), vec4(0.24154,0.920079,0,0)},
{ vec4(-0.0362691,0.0421007,-0.0258182,1), vec4(0,0,0,0), vec4(0.282582,0.752376,0,0)},
{ vec4(-0.0277647,0.0194327,-0.0241263,1), vec4(0,0,0,0), vec4(0.333563,0.616491,0,0)},
{ vec4(-0.0182498,0,-0.021876,1), vec4(0,0,0,0), vec4(0.3906,0.5,0,0)},
{ vec4(-0.0290158,0.0677865,-0.0379312,1), vec4(0,0,0,0), vec4(0.326063,0.906351,0,0)},
{ vec4(-0.0212823,0.0399525,-0.035765,1), vec4(0,0,0,0), vec4(0.372422,0.739498,0,0)},
{ vec4(-0.011279,0.0182498,-0.033155,1), vec4(0,0,0,0), vec4(0.432387,0.6094,0,0)},
{ vec4(0,0,-0.0300375,1), vec4(0,0,0,0), vec4(0.5,0.5,0,0)},
{ vec4(-0.0751651,-0.0751651,-0.00256251,1), vec4(0,0,0,0), vec4(0.0494178,0.0494178,0,0)},
{ vec4(-0.0474252,-0.0725366,-0.00424318,1), vec4(0,0,0,0), vec4(0.215707,0.0651743,0,0)},
{ vec4(-0.0225143,-0.0700766,-0.00638528,1), vec4(0,0,0,0), vec4(0.365037,0.0799212,0,0)},
{ vec4(2e-012,-0.0677865,-0.00891536,1), vec4(0,0,0,0), vec4(0.5,0.0936493,0,0)},
{ vec4(-0.0725366,-0.0474252,-0.00424318,1), vec4(0,0,0,0), vec4(0.0651743,0.215707,0,0)},
{ vec4(-0.044813,-0.044813,-0.0069796,1), vec4(0,0,0,0), vec4(0.231365,0.231365,0,0)},
{ vec4(-0.0208421,-0.0421007,-0.0103912,1), vec4(0,0,0,0), vec4(0.375061,0.247624,0,0)},
{ vec4(0,-0.0399525,-0.0144827,1), vec4(0,0,0,0), vec4(0.5,0.260502,0,0)},
{ vec4(-0.0700766,-0.0225143,-0.00638528,1), vec4(0,0,0,0), vec4(0.0799212,0.365037,0,0)},
{ vec4(-0.0421007,-0.0208421,-0.0103912,1), vec4(0,0,0,0), vec4(0.247624,0.375061,0,0)},
{ vec4(-0.0194327,-0.0194327,-0.0157943,1), vec4(0,0,0,0), vec4(0.383509,0.383509,0,0)},
{ vec4(0,-0.0182498,-0.021876,1), vec4(0,0,0,0), vec4(0.5,0.3906,0,0)},
{ vec4(-0.0677865,0,-0.00891536,1), vec4(0,0,0,0), vec4(0.0936493,0.5,0,0)},
{ vec4(-0.0399525,0,-0.0144827,1), vec4(0,0,0,0), vec4(0.260502,0.5,0,0)},
{ vec4(-0.0182498,0,-0.021876,1), vec4(0,0,0,0), vec4(0.3906,0.5,0,0)},
{ vec4(0,0,-0.0300375,1), vec4(0,0,0,0), vec4(0.5,0.5,0,0)},
{ vec4(0.0751651,-0.0834088,-0.0108063,1), vec4(0,0,0,0), vec4(0.950582,0,0,0)},
{ vec4(0.0725366,-0.0609865,-0.0178045,1), vec4(0,0,0,0), vec4(0.934826,0.134412,0,0)},
{ vec4(0.0700766,-0.0431156,-0.0269866,1), vec4(0,0,0,0), vec4(0.920079,0.24154,0,0)},
{ vec4(0.0677865,-0.0290158,-0.0379312,1), vec4(0,0,0,0), vec4(0.906351,0.326063,0,0)},
{ vec4(0.0474252,-0.0787311,-0.0104377,1), vec4(0,0,0,0), vec4(0.784293,0.028041,0,0)},
{ vec4(0.044813,-0.0550038,-0.0171704,1), vec4(0,0,0,0), vec4(0.768635,0.170276,0,0)},
{ vec4(0.0421007,-0.0362691,-0.0258182,1), vec4(0,0,0,0), vec4(0.752376,0.282582,0,0)},
{ vec4(0.0399525,-0.0212823,-0.035765,1), vec4(0,0,0,0), vec4(0.739498,0.372422,0,0)},
{ vec4(0.0225143,-0.0735299,-0.00983858,1), vec4(0,0,0,0), vec4(0.634963,0.0592201,0,0)},
{ vec4(0.0208421,-0.0477143,-0.0160048,1), vec4(0,0,0,0), vec4(0.624939,0.213973,0,0)},
{ vec4(0.0194327,-0.0277647,-0.0241263,1), vec4(0,0,0,0), vec4(0.616491,0.333563,0,0)},
{ vec4(0.0182498,-0.011279,-0.033155,1), vec4(0,0,0,0), vec4(0.6094,0.432387,0,0)},
{ vec4(0,-0.0677865,-0.00891536,1), vec4(0,0,0,0), vec4(0.5,0.0936493,0,0)},
{ vec4(0,-0.0399525,-0.0144827,1), vec4(0,0,0,0), vec4(0.5,0.260502,0,0)},
{ vec4(0,-0.0182498,-0.021876,1), vec4(0,0,0,0), vec4(0.5,0.3906,0,0)},
{ vec4(0,0,-0.0300375,1), vec4(0,0,0,0), vec4(0.5,0.5,0,0)},
};

GLuint test_patch_indices[] = {
0, 1, 2, 3, 
4, 5, 6, 7, 
8, 9, 10, 11, 
12, 13, 14, 15, 
16, 17, 18, 19, 
20, 21, 22, 23, 
24, 25, 26, 27, 
28, 29, 30, 31, 
32, 33, 34, 35, 
36, 37, 38, 39, 
40, 41, 42, 43, 
44, 45, 46, 47, 
48, 49, 50, 51, 
52, 53, 54, 55, 
56, 57, 58, 59, 
60, 61, 62, 63, 
64, 65, 66, 67, 
68, 69, 70, 71, 
72, 73, 74, 75, 
76, 77, 78, 79, 
};

void Initialize(int, char*[]);
void ResizeFunction(int, int);
void RenderFunction(void);
void TimerFunction(int);
void IdleFunction(void);
void KeyboardFunction(unsigned char, int, int);
void MouseMotionFunction(int x, int y);
void MouseFunction(int button, int state, int x, int y);
void Cleanup(void);
void CreateObjects(void);



void CreateShaders(void);

void CreateShadowMap(void);
void CreateComputeResource(void);

void CreateGUI(void);

GLuint wraptex = 0;

int main(int argc, char* argv[])
{
	Initialize(argc, argv);

	glutMainLoop();
	
	exit(EXIT_SUCCESS);
}

void Initialize(int argc, char* argv[])
{
	GLenum GlewInitResult;

	glutInit(&argc, argv);
	// Init Context	
	glutInitContextVersion(4, 3);
	glutInitContextProfile(GLUT_CORE_PROFILE);
	glutInitContextFlags(GLUT_FORWARD_COMPATIBLE | GLUT_DEBUG);

	glutSetOption(
		GLUT_ACTION_ON_WINDOW_CLOSE,
		GLUT_ACTION_GLUTMAINLOOP_RETURNS
	);
	
	// Init Window
	glutInitWindowSize(CurrentWidth, CurrentHeight);
	glutSetOption(GLUT_MULTISAMPLE, 16);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA | GLUT_MULTISAMPLE);

	WindowHandle = glutCreateWindow(WINDOW_TITLE_PREFIX);

	if(WindowHandle < 1) {
		fprintf(
			stderr,
			"ERROR: Could not create a new rendering window.\n"
		);
		exit(EXIT_FAILURE);
	}

	glewExperimental = GL_TRUE;
	GlewInitResult = glewInit();
	initDebugOutput();

	// init GUI
	TwInit(TW_OPENGL_CORE, NULL);

	// set callbacks
	glutReshapeFunc(ResizeFunction);
	glutDisplayFunc(RenderFunction);
	glutIdleFunc(IdleFunction);
	glutCloseFunc(Cleanup);
	glutKeyboardFunc(KeyboardFunction);
	glutMotionFunc(MouseMotionFunction);
	glutMouseFunc(MouseFunction);
	


	if (GLEW_OK != GlewInitResult) {
		fprintf(
			stderr,
			"ERROR: %s\n",
			glewGetErrorString(GlewInitResult)
		);
		exit(EXIT_FAILURE);
	}

	fprintf(stdout, "Status: Using GLEW %s\n", glewGetString(GLEW_VERSION));

	fprintf(
		stdout,
		"INFO: OpenGL Version: %s\n",
		glGetString(GL_VERSION)
	);

	int max_shared_variable_size;
	glGetIntegerv(GL_MAX_COMPUTE_SHARED_MEMORY_SIZE, &max_shared_variable_size);
		fprintf(
		stdout,
		"GL_MAX_COMPUTE_SHARED_MEMORY_SIZE: %d\n",
		max_shared_variable_size
	);

	// Init OpenGL
	glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
    glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glEnable(GL_MULTISAMPLE);
	glMinSampleShading(16);

	// Init Shaders and Vertices
	CreateShaders();
	CreateObjects();


	CreateGUI();

	// Init Matrix	
	// ModelMatrix = glm::mat4(1.0);	// Identity Matrix

	camera.pos = glm::vec3(1.8,1.2f,-4.5);
	camera.center = glm::vec3(0,0,0.0f);
	camera.up = glm::vec3(0,0.0,1.0f);
	camera.fovy = 45.0;
	camera.nearplane = 0.01;
	camera.farplane = 10.0;
	camera.ratio = 1.0;

	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

}

void updatCameraMatrix()
{
	ViewMatrix = glm::lookAt(camera.pos,	// eye
						camera.center,	// center
						camera.up	// up
			);

	ProjectionMatrix = glm::perspective(camera.fovy, camera.ratio, camera.nearplane, camera.farplane);
}

void KeyboardFunction(unsigned char Key, int X, int Y)
{
	X; Y; // Resolves warning C4100: unreferenced formal parameter

	
	if( show_ui && TwEventKeyboardGLUT(Key,X, Y) )  // send event to AntTweakBar
    { 
		return;
    }

	switch (Key)
	{
    case 'w':
		forwadCamera(&camera, 0.2);
        break;
	case 's':
		forwadCamera(&camera, -0.2);
        break;
	case 'a':
		panCamera(&camera, glm::vec2(-0.02,0));
		break;
	case 'd':
		panCamera(&camera, glm::vec2(0.02,0));
		break;
    case 'f':
		wireframe_mode = false;
        break;
	case 'F':
		wireframe_mode = true;
        break;
	case 'c':
		use_compute = !use_compute;
		break;
	case 'u':
		show_ui = !show_ui;
		break;
	default:
		break;
	}
}


int old_x = 0;
int old_y = 0;
bool middleButtonDown = false;
bool leftButtonDown = false;
bool rightButtonDown = false;

int old_selected_patch_id = 0;

void MouseFunction(int button, int state, int x, int y)
{
	old_x = x;
	old_y = y;

	cout << x << " " << y << endl;

	if( show_ui && TwEventMouseButtonGLUT(button, state, x, y) )  // send event to AntTweakBar
    { 
		return;
    }

	if(button == GLUT_MIDDLE_BUTTON){
		middleButtonDown = (state == GLUT_DOWN) ? true : false;
	}
	if(button == GLUT_LEFT_BUTTON){
		cout << "left " << state << endl;
		leftButtonDown = (state == GLUT_DOWN) ? true : false;


		if(glutGetModifiers() & GLUT_ACTIVE_CTRL)
		{
		}
	}

	if(button == GLUT_RIGHT_BUTTON){
		cout << "left " << state << endl;
		rightButtonDown = (state == GLUT_DOWN) ? true : false;
	}
}


void MouseMotionFunction(int x, int y)
{
	
	int dx = x-old_x;
	int dy = y-old_y;

	old_x = x;
	old_y = y;

	if( show_ui && TwEventMouseMotionGLUT(x, y) )  // send event to AntTweakBar
    { 
		return;
    }


	if(middleButtonDown)
		panCamera(&camera,glm::vec2(-dx*0.001,dy*0.001));
	else if(leftButtonDown){
		rotateCamera(&camera, glm::vec2(-dx*0.01,dy*0.01));
	}
	else if(rightButtonDown){
		zoomCamera(&camera, pow(1.01,dy));
	}

}

void ResizeFunction(int Width, int Height)
{
	CurrentWidth = Width;
	CurrentHeight = Height;
	glViewport(0, 0, CurrentWidth, CurrentHeight);

	// resize GUI
	TwWindowSize(CurrentWidth, CurrentHeight);
}


void CreateGUI()
{
	TwBar *myBar;
	myBar = TwNewBar("Control Panel");

	TwAddVarRW(myBar, "WireframeMode", TW_TYPE_BOOLCPP, &wireframe_mode, "");
	TwAddVarRW(myBar, "UseCompute", TW_TYPE_BOOLCPP, &use_compute, "");

	TwAddSeparator(myBar,"Material","");
	TwAddVarRW(myBar, "Ka", TW_TYPE_COLOR3F, &Ka[0], "");
	TwAddVarRW(myBar, "Ks", TW_TYPE_COLOR3F, &Ks[0], "");
	TwAddVarRW(myBar, "Kd", TW_TYPE_COLOR3F, &Kd[0], "");


	TwAddSeparator(myBar,"DEBUG","");
}


void RenderFunction(void)
{	
	updatCameraMatrix();
	glDisable(GL_CULL_FACE);    

	if(wireframe_mode)
		glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
	else
		glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	renderer->renderSurface(test_surface, CurrentWidth, is_first_frame, use_compute, Ka, Kd, Ks, ViewMatrix * ModelMatrix, ProjectionMatrix);
	
	
	// Draw GUI
	if(show_ui)
	{
		glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
		TwDraw();
	}

	is_first_frame = false;

	glutSwapBuffers();
	glutPostRedisplay();
}

void IdleFunction(void)
{
	glutPostRedisplay();
}

void Cleanup(void)
{

}

void CreateObjects()
{
	renderer = new SurfaceRenderer();

	test_surface = new GLSurface();
	test_surface->init_patches(test_patch_vertices, sizeof(test_patch_vertices)/sizeof(Vertex), test_patch_indices, sizeof(test_patch_indices)/sizeof(GLuint));
}


void CreateShaders(void)
{
	SurfaceRenderer::init_shaders();
}
