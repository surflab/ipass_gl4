#ifndef COMMON_H
#define COMMON_H

// memory related macro

#define ZERO_MEM(a) memset(a, 0, sizeof(a))

#define ARRAY_SIZE_IN_ELEMENTS(a) (sizeof(a)/sizeof(a[0]))

#define INVALID_OGL_VALUE 0xFFFFFFFF

#define SAFE_DELETE(p) if (p) { delete p; p = NULL; }

#define GLExitIfError()                                                          \
{                                                                               \
    GLenum Error = glGetError();                                                \
                                                                                \
    if (Error != GL_NO_ERROR) {                                                 \
        printf("OpenGL error in %s:%d: 0x%x\n", __FILE__, __LINE__, Error);     \
        exit(0);                                                                \
    }                                                                           \
}


// constants
const float MAX_FLOAT = 10000.0f;


// GL related header
#define GLCheckError() (glGetError() == GL_NO_ERROR)

#include <GL/glew.h>
#include <GL/freeglut.h>

#define GLM_SWIZZLE 
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>

#include <vector>
#include <cstdio>

// GL related structure
typedef struct
{
	glm::vec4 XYZW;	// position
	glm::vec4 RGBA;	// color
	glm::vec4 ST;	// tex_coord
	// glm::vec4 Du;	// tex_coord
	// glm::vec4 Dv;	// tex_coord
} Vertex;

typedef struct
{
	std::vector<Vertex> vertices;
	std::vector<GLushort> indices;
} Mesh;

typedef struct
{
	glm::vec3 pos;
	glm::vec3 up;
	glm::vec3 center;
	float ratio;
	float nearplane;
	float farplane;
	float fovy;
} Camera;

typedef struct
{
    GLuint vao_id;
	GLuint vbuffer_id;
	GLuint ibuffer_id;
	GLuint num_vertices;
} glObject;

typedef struct
{
	float POSITION[3];
	float DIRECTION[3];
	float angel;

} glLight;



#endif
