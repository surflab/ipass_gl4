#ifndef SURFACE_H
#define SURFACE_H

#include <vector>

#include "setting.h"
#include "common.h"
#include "glresource.h"
#include "glslprogram.h"
#include "utils.h"


// Surface for iPass
class GLSurface : public glObject
{
public:


public:
	float texel_size;

	float tess_level;

	bool selected;

	std::vector<Vertex> patch_vertices;
	std::vector<glm::ivec4> patch_connectivity;

	// for iPass
	GLTexture *patch_tess_level_texture;
	GLTexture *connectivity_texture;

	int num_patches;
	glm::vec3 bbox[2];

public:

	GLSurface() : selected(false)
	{
	}

	~GLSurface()
	{
	}

	void init_patches(Vertex* patch_vertices, int num_vertices, GLuint *patch_indices, int num_indices)
	{
		this->num_patches = num_vertices / 16;

		CreateVBO(patch_vertices, num_vertices, patch_indices, num_indices, &this->vao_id, &this->vbuffer_id, &this->ibuffer_id);
		this->num_vertices = num_vertices;

		this->patch_vertices.insert(this->patch_vertices.end(), patch_vertices, patch_vertices + num_vertices);

		// update bounding box
		bbox[0] = glm::vec3(MAX_FLOAT, MAX_FLOAT, MAX_FLOAT);
		bbox[1] = glm::vec3(-MAX_FLOAT, -MAX_FLOAT, -MAX_FLOAT);

		for(int i = 0; i < this->patch_vertices.size(); i++)
		{
			bbox[0] = glm::min(bbox[0], vec3(this->patch_vertices[i].XYZW.xyz()));
			bbox[1] = glm::max(bbox[1], vec3(this->patch_vertices[i].XYZW.xyz()));
		}


		// calculate connectivity
		this->patch_connectivity = calConnectivityOfPatches(&patch_vertices[0], num_vertices, &patch_indices[0], num_indices);

		patch_tess_level_texture = createComputeTexture1D(this->num_patches, GLResource::FLOAT);
		connectivity_texture = createComputeTexture1D(this->num_patches, GLResource::INT4);

		glBindTexture(GL_TEXTURE_1D, this->connectivity_texture->getHandle());
		glTexSubImage1D(GL_TEXTURE_1D, 0, 0, this->patch_connectivity.size(), GL_RGBA_INTEGER, GL_INT, &this->patch_connectivity[0]);
	}


};


#endif
