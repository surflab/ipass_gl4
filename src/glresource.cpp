#include "glresource.h"

GLTexture* createComputeTexture2DMSAA(int width, int height, GLResource::GLResourceType type)
{
	GLuint handle;
	glGenTextures(1,&handle);
	glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, handle);

	switch(type){

	case GLResource::INT:
		glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, 16, GL_R32I, width, height, GL_TRUE);
		break;
	case GLResource::UINT:
		glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, 16, GL_R32UI, width, height, GL_TRUE);
		break;
	case GLResource::FLOAT:
		glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, 16, GL_R32F, width, height, GL_TRUE);
		break;
	case GLResource::FLOAT4:
		glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, 16, GL_RGBA, width, height, GL_TRUE);
		break;
	case GLResource::INT4:
		glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, 16, GL_RGBA32I, width, height, GL_TRUE);
		break;

	default:
		// unsupported type
		glDeleteTextures(1, &handle);
		return NULL;
	}

	glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, 0);

	return new GLTexture2D(handle, type, width, height);
}

GLTexture* createComputeTexture2D(int width, int height, GLResource::GLResourceType type)
{
	GLuint handle;
	glGenTextures(1,&handle);
	glBindTexture(GL_TEXTURE_2D, handle);

	switch(type){

	case GLResource::INT:
		glTexImage2D(GL_TEXTURE_2D, 0, GL_R32I, width, height, 0, GL_RED_INTEGER, GL_INT, NULL);
		break;
	case GLResource::UINT:
		glTexImage2D(GL_TEXTURE_2D, 0, GL_R32UI, width, height, 0, GL_RED_INTEGER, GL_UNSIGNED_INT, NULL);
		break;
	case GLResource::FLOAT:
		glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, width, height, 0, GL_RED, GL_FLOAT, NULL);
		break;
	case GLResource::FLOAT4:
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_FLOAT, NULL);
		break;
	case GLResource::INT4:
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32I, width, height, 0, GL_RGBA_INTEGER, GL_INT, NULL);
		break;

	default:
		// unsupported type
		glDeleteTextures(1, &handle);
		return NULL;
	}
        
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glBindTexture(GL_TEXTURE_2D, 0);

	return new GLTexture2D(handle, type, width, height);
}

GLTexture* createComputeTexture1D(int width, GLResource::GLResourceType type)
{
	GLuint handle;
	glGenTextures(1,&handle);
	glBindTexture(GL_TEXTURE_1D, handle);

	switch(type){
	case GLResource::INT:
		glTexImage1D(GL_TEXTURE_1D, 0, GL_R32I, width, 0, GL_RED_INTEGER, GL_INT, NULL);
		break;
	case GLResource::UINT:
		glTexImage1D(GL_TEXTURE_1D, 0, GL_R32UI, width, 0, GL_RED_INTEGER, GL_UNSIGNED_INT, NULL);
		break;
	case GLResource::FLOAT:
		glTexImage1D(GL_TEXTURE_1D, 0, GL_R32F, width, 0, GL_RED, GL_FLOAT, NULL);
		break;
	case GLResource::FLOAT4:
		glTexImage1D(GL_TEXTURE_1D, 0, GL_RGBA32F, width, 0, GL_RGBA, GL_FLOAT, NULL);
		break;
	case GLResource::INT4:
		glTexImage1D(GL_TEXTURE_1D, 0, GL_RGBA32I, width, 0, GL_RGBA_INTEGER, GL_INT, NULL);
		break;

	default:
		// unsupported type
		glDeleteTextures(1, &handle);
		return NULL;
	}
        
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glBindTexture(GL_TEXTURE_1D, 0);

	return new GLTexture1D(handle, type, width);
}

GLBuffer* createBuffer(int size, GLuint usage)
{
	GLuint handle;
	glGenBuffers(1,&handle);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, handle);
	glBufferData(GL_SHADER_STORAGE_BUFFER, size, NULL, usage);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

	return new GLBuffer(handle, size);	
}

void CreateVBO(Vertex* Vertices, int num_vertices, GLuint* Indices, int num_indices, GLuint *VaoId, GLuint *BufferId, GLuint *IndexBufferId)
{
	GLenum ErrorCheckValue = glGetError();
	const size_t VertexSize = sizeof(Vertex);
	const size_t BufferSize = num_vertices * VertexSize;
	const size_t RgbOffset = sizeof(float)*4;

    // Create Vertex Array Object
	glGenVertexArrays(1, VaoId);
	glBindVertexArray(*VaoId);
	
	// Create Buffer for vertex data
	glGenBuffers(1, BufferId);
	glBindBuffer(GL_ARRAY_BUFFER, *BufferId);
	glBufferData(GL_ARRAY_BUFFER, BufferSize, Vertices, GL_STATIC_DRAW);

    // Assign vertex attributes
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, RGBA));
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, ST));

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	// Create Buffer for indics
	glGenBuffers(1, IndexBufferId);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *IndexBufferId);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, num_indices * sizeof(GLuint), Indices, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

	ErrorCheckValue = glGetError();
	if (ErrorCheckValue != GL_NO_ERROR)
	{
		fprintf(
			stderr,
			"ERROR: Could not create a VBO: %s \n",
			gluErrorString(ErrorCheckValue)
		);

		exit(-1);
	}
}

void DestroyVBO(GLuint* BufferId, GLuint *IndexBufferId)
{
	GLenum ErrorCheckValue = glGetError();

	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(0);
	
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glDeleteBuffers(1, BufferId);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glDeleteBuffers(2, IndexBufferId);

	ErrorCheckValue = glGetError();
	if (ErrorCheckValue != GL_NO_ERROR)
	{
		fprintf(
			stderr,
			"ERROR: Could not destroy the VBO: %s \n",
			gluErrorString(ErrorCheckValue)
		);

		exit(-1);
	}
}
