#ifndef PATCH_H
#define PATCH_H

#include "setting.h"
#include "common.h"
#include <vector>

std::vector<float> determine_patch_tess_levels(Vertex *vertices, int num_patches, int degu, int degv, const glm::mat4 &MVP, float pixel_size);

#endif